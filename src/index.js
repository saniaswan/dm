import "babel-polyfill";
import React from "react";
import ReactDOM from "react-dom";
import { StyleRoot } from "radium";
import registerServiceWorker from "./registerServiceWorker";
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import injectTapEventPlugin from "react-tap-event-plugin";
import BrowserRouter from "./BrowserRouter";
import { Provider } from "react-redux";
import configureStore from "./store/configureStore";
import configureAxios from "./api/axiosDefault";
import "bootstrap/dist/css/bootstrap.min.css";
import { muiTheme } from "./styles/theme";
import "./App.css";
import "font-awesome/css/font-awesome.css";
import LogRocket from 'logrocket';
import Raven from 'raven';



// Configure our CrashReporting and Logging and Session Replay tools
LogRocket.init('nova-jc6qx/cheqs');
Raven.setDataCallback(function (data) {
  data.extra.sessionURL = LogRocket.sessionURL;
  return data;
});

configureAxios();
const store = configureStore();

injectTapEventPlugin();

ReactDOM.render(
  <MuiThemeProvider muiTheme={muiTheme}>
    <Provider store={store}>
      <StyleRoot>
        <BrowserRouter />
      </StyleRoot>
    </Provider>
  </MuiThemeProvider>,
  document.getElementById("root")
);
registerServiceWorker();
