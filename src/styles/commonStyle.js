import {
  maxmedia520,
  maxmedia420,
  maxmedia767,
  maxmedia1024,
  minmedia767
} from "./js/mediaQueries";
import typography from "./theme/typography";
import {
  white,
  appCanvosColor,
  colorPalette,
  gray400,
  gray200,
  gray600,
  cardBorder
} from "./theme/colors";
export const appColorPrimary = {
  backgroundColor: "#486298",
  color: "#fff"
};
export const appColorSecondary = {
  backgroundColor: "#f0846f",
  color: "#fff"
};

export const appErrorColor = {
  backgroundColor: "#f0846f",
  color: "#fff"
};

export const appBodyContainer = {
  maxWidth: "1200px",
  margin: "0px auto",
  position: "relative",
  top: "-47px",
  zIndex: 500,
  [maxmedia767]: {
    top: "0px"
  }
};
export const whiteBackGround = {
  backgroundColor: "#fff",
  color: "#333"
};

export const fontColor = {
  color: "#333"
};

export const heading1 = {
  fontSize: typography.headingFontSizeDesktop,
  [maxmedia520]: { fontSize: typography.headingFontSizeDevice }
};

export const textFieldFontStyle = {
  fontSize: 14,
  color: "#333",
  fontWeight: 400
};
export const textFieldFontFoucsStyle = {
  color: "#00BCD4"
};

export const cardTitle = {
  fontSize: 20,
  fontWeight: 100,
  color: white
};

export const cardHeader = {
  padding: 21,
  backgroundColor: colorPalette.primary2Color
};

export const borderBottom = {
  borderBottom: `1px solid ${colorPalette.primary2Color}`
};

export const containerPadding = {
  padding: "100px 20px",
  [maxmedia767]: {
    padding: "70px 20px"
  },
  [maxmedia520]: {
    padding: "50px 20px"
  },
  [maxmedia420]: {
    padding: "30px 20px"
  }
};

export const containerCompPadding = {
  minHeight: "500px",
  padding: "50px 20px 100px",
  [maxmedia520]: {
    padding: "50px 20px 70px"
  },
  [maxmedia420]: {
    padding: "50px 20px 70px"
  }
};

export const containerCompPaddingDesk = {
  padding: "50px 23px 100px 40px",

  paddingTop: "45px",
  overflowX: "hidden",
  minHeight: "500px"
};
export const containerCompPaddingDeskCalendar = {
  padding: "50px 23px 100px 23px",

  paddingTop: "45px",
  overflowX: "hidden",
  minHeight: "500px"
};
export const containerCompPaddingDeskWithAction = {
  [minmedia767]: {
    padding: "50px 20px 100px"
  },
  paddingRight: "initial",
  paddingLeft: "initial",
  paddingTop: "45px",
  paddingBottom: "75px",
  overflowX: "hidden",
  minHeight: "500px"
};

export const contentHeading = {
  fontSize: 24,
  fontWeight: 500
};

export const fullWidth = {
  width: "100%"
};
export const threeColumnWidth = {
  width: "75%",
  [maxmedia1024]: {
    width: "100%"
  }
};
export const overlayAll = {
  position: "fixed",
  top: 0,
  right: 0,
  left: 0,
  bottom: 0,
  background: "white",
  zIndex: 999
};

export const appBase = {
  backgroundColor: appCanvosColor
};

export const PanelSpacing = {
  padding: "100px 20px",
  [maxmedia520]: {
    padding: "70px 20px"
  },
  [maxmedia420]: {
    padding: "50px 20px"
  }
};

export const subHeading = {
  fontSize: typography.subHeadingFontSizeDesktop,
  color: gray600,
  width: "100%",
  position: "relative",
  lineHeight: "18px",
  marginLeft: "3px",
  [maxmedia520]: {
    fontSize: typography.subHeadingFontSizeDevice,
    lineHeight: "20px"
  }
};

export const titleSubHeadingPosition = {
  marginLeft: "36px",
  color: gray600
};

export const whiteTitleSubHeadingPosition = {
  color: "white",
  marginLeft: "55px",
  marginTop: "-18px",
  display: "block"
};

export const contentPaddingTop = {
  paddingTop: 50
};

export const contentPaddingTopList = {
  paddingTop: 40
};

export const contentPaddingTopListRisk = {
  paddingTop: 40,
  [maxmedia767]: {
    paddingTop: 0
  }
};
export const riskChartWindow = {
  overflowY: "auto"
};
export const riskChartWindowWidth = {
  width: "80%",
  maxWidth: "848px"
};
export const floatingAddBtn = {
  position: "absolute",
  right: 40,
  bottom: "-15px"
};
export const marginBtnRight = {
  marginRight: 5
};

export const hintStyle = {
  fontSize: 12,
  bottom: "-13px"
};

export const formActionPositionMax = {
  marginTop: 100,
  [maxmedia520]: {
    marginTop: 50
  }
};

export const formActionPositionMin = {
  marginTop: 50
};

export const topBarTitleColor = {
  color: "black",
  [maxmedia767]: {
    color: "white"
  }
};
export const supportingText2 = {
  marginTop: 37,
  width: "90%"
};

export const noTextDecoration = {
  textDecoration: "none"
};

export const progressBar = {
  height: 30,
  backgroundColor: gray400
};
export const progressBarCompleted = {
  color: white,
  position: "relative",
  top: 33,
  fontSize: 12,
  zIndex: 9,
  left: 8
};

export const centerIcon = {
  textShadow: "0 1px 0 rgba(0, 0, 0, 0.1)",
  textAlign: "center",
  display: "block"
};

export const centerElement = {
  margin: "0 auto",
  display: "inherit"
};

export const marginTop = {
  marginTop10: {
    marginTop: 10
  },
  marginTop20: {
    marginTop: 20
  }
};

export const marginBottom = {
  marginBottom05: {
    marginBottom: 5
  },
  marginBottom10: {
    marginBottom: 10
  },
  marginBottom20: {
    marginBottom: 20
  }
};
export const marginRight = {
  marginRight5: {
    marginRight: 5
  },
  marginRight10: {
    marginRight: 10
  },
  marginRight20: {
    marginRight: 20
  }
};

export const paddingRight = {
  paddingRight10: {
    paddingRight: 10
  },
  padding20: {
    paddingRight: 20
  }
};

export const marginLeft = {
  marginLeft10: {
    marginLeft: 10
  },
  marginLeft20: {
    marginLeft: 20
  }
};

export const displayNone = {
  display: "none"
};

export const displayNoneOnLow = {
  [maxmedia767]: {
    display: "none"
  }
};

export const displayNoneOnHigh = {
  [minmedia767]: {
    display: "none"
  }
};

export const centerText = {
  textAlign: "center"
};

export const centerLeft = {
  textAlign: "left"
};

export const display1 = {
  fontSize: "34px"
};

export const title1 = {
  fontSize: "20px",
  fontWeight: 400
};

export const titleBox = {
  padding: "40px 30px",
  width: "100%",
  backgroundColor: gray200
};

export const titleBox2 = {
  padding: "40px",
  width: "100%"
};

export const textMargin = {
  [maxmedia767]: {
    marginLeft: "10px"
  }
};

export const headingPostionChange = {
  headingPostion: {
    [maxmedia767]: {
      padding: "initial",
      backgroundColor: "transparent",
      position: "absolute",
      top: "-53px",
      left: "60px",
      color: "white",
      width: "75%"
    },
    [maxmedia520]: {
      width: "60%"
    },
    [maxmedia420]: {
      width: "55%"
    }
  },
  headingIcon: {
    [maxmedia767]: {
      display: "none"
    }
  },
  headingTypo: {
    [maxmedia767]: {
      color: "white",
      fontSize: 18,

      overflow: "hidden",
      whiteSpace: "nowrap",
      textOverflow: "ellipsis"
    }
  },
  desktopPostion: {
    marginRight: "10px",
    top: "7px",
    position: "relative"
  }
};

export const actionBox = {
  marginTop: "-45px",
  marginRight: "10px"
};

export const deleteActionBtnPosition = {
  marginTop: "-40px",
  marginRight: "10px",
  float: "right"
};

export const deleteActionBtnPosition2 = {
  marginTop: "-50px",
  marginRight: "10px",
  float: "right"
};

export const noBold = {
  fontWeight: "normal"
};

export const Bold = {
  fontWeight: "bold"
};

export const clearBoth = {
  clear: "both"
};

export const innerPaddingActionBox = {
  padding: "10px 15px"
};

export const flatButtonOnlyText = {
  defaultStyle: {
    padding: "0px",
    minWidth: "initial",
    marginRight: 20
  },
  labelStyle: {
    padding: "2px"
  }
};

export const flatIconButtonColor = {
  color: "rgb(51, 51, 51)"
};

export const subHeadingBlack = {
  fontSize: 15,
  [maxmedia767]: {
    fontSize: 16
  }
};

export const body2 = {
  fontSize: 13,
  [maxmedia767]: {
    fontSize: 14
  }
};

export const topPaddingOnSmall = {
  [maxmedia767]: {
    paddingTop: 20
  }
};

export const flatIconButtonSmall = {
  width: "initial",
  height: "initial",
  padding: 0,
  color: "black"
};
export const chevronIconBtn = {
  width: 56,
  height: 56
};

export const displayOnTop = {
  zIndex: 9999
};
export const relative = {
  position: "relative"
};

export const floatLeft = {
  float: "left"
};
export const noPaddingIconBtn = {
  minWidth: "auto",
  color: "#333"
};
export const noPaddingIconTextBtn = {
  minWidth: "auto",
  color: "rgb(72, 98, 152)"
};
export const paddingBottom = {
  paddingBottom20: {
    paddingBottom: 20
  }
};

export const cardBorderBottom = {
  borderBottom: `5px solid ${cardBorder}`
};

export const twoActionButtonWrap = {
  textAlign: "left",
  borderTop: "1px solid",
  borderColor: gray600,
  padding: "10px 20px",
  cursor: "auto"
};
export const twoActionBlueButtonWrap = {
  textAlign: "right",
  padding: "0 20px",
  cursor: "auto",
  background: "rgb(120, 143, 201)"
};

export const contentRight = {
  textAlign: "right"
};

export const contentCenter = {
  textAlign: "center"
};

export const contentRightLeft = {
  textAlign: "right",
  [maxmedia767]: {
    textAlign: "left"
  }
};
export const smallFlateButton = {
  padding: "0px",
  minWidth: "48px"
};

export const completeWrape = {
  border: "1px solid green"
};

export const overdueWrape = {
  border: "1px solid red"
};

export const upcomingWrape = {
  border: "1px solid #788fc9"
};

export const ellipsisText = {
  whiteSpace: "nowrap",
  overflow: "hidden",
  textOverflow: "ellipsis",
  width: "100%"
};

export const upperCase = {
  textTransform: "uppercase"
};

export const inlineBlock = {
  display: "inline-block"
};

export const actionItemPadding = {
  padding: "50px 20px 0px"
};

export const topRightCorner = {
  top: 5,
  right: 20,
  position: "absolute"
};

export const extraSmall = {
  smallIcon: {
    width: 16,
    height: 16
  },
  small: {
    width: 22,
    height: 22,
    padding: 0
  }
};

export const mediumIcon = {
  Icon: {
    width: 18,
    height: 18
  }
};
export const radioBtn = {
  Label: {
    fontWeight: "normal",
    fontSize: 16
  },
  radioButton: {
    marginBottom: 16
  }
};
