import { colorPalette, gray200, gray300 } from "./colors";
import typography from "./typography";
import getMuiTheme from "material-ui/styles/getMuiTheme";

export const muiTheme = getMuiTheme({
  palette: {
    primary1Color: colorPalette.primary1Color,
    primary2Color: colorPalette.primary2Color,
    primary3Color: colorPalette.primary3Color,
    accent1Color: colorPalette.secondary1Color,
    accent2Color: colorPalette.secondary2Color,
    accent3Color: colorPalette.secondary3Color,
    textColor: colorPalette.textColor,
    secondaryTextColor: colorPalette.secondaryTextColor,
    alternateTextColor: colorPalette.alternateTextColor,
    //canvasColor: "#000",
    // borderColor: _colors.grey300,
    // disabledColor: (0, _colorManipulator.fade)(_colors.darkBlack, 0.3),
    pickerHeaderColor: colorPalette.pickerHeaderColor

    // clockCircleColor: (0, _colorManipulator.fade)(_colors.darkBlack, 0.07),

    // shadowColor: _colors.fullBlack
  },
  appBar: {
    color: colorPalette.primary1Color
  },
  textField: {
    textColor: colorPalette.textColor,
    focusColor: colorPalette.primary2Color,
    backgroundColor: "transparent",
    fontSize: 12,
    fontWeight: typography.fontWeightNormal
  },
  datePicker: {
    textColor: colorPalette.textColor,
    focusColor: colorPalette.secondary1Color,
    backgroundColor: "transparent",
    fontSize: 12,
    fontWeight: typography.fontWeightNormal
  },
  raisedButton: {},
  tabs: {
    backgroundColor: colorPalette.primary2Color,
    textColor: colorPalette.alternateTextColor
  },
  checkbox: {
    checkedColor: colorPalette.secondary1Color
  },
  toggle: {
    trackOnColor: colorPalette.secondary2Color,
    trackOffColor: gray300,
    thumbOnColor: colorPalette.secondary1Color,
    thumbOffColor: gray200,
  }
});
