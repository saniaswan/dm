export const appCanvosColor = "#f5f4f4";
export const black = "#000000";
export const white = "#ffffff";

export const transparent = "rgba(0, 0, 0, 0)";
export const fullBlack = "rgba(0, 0, 0, 1)";
export const darkBlack = "rgba(0, 0, 0, 0.87)";
export const lightBlack = "rgba(0, 0, 0, 0.54)";
export const minBlack = "rgba(0, 0, 0, 0.26)";
export const faintBlack = "rgba(0, 0, 0, 0.12)";
export const fullWhite = "rgba(255, 255, 255, 1)";
export const darkWhite = "rgba(255, 255, 255, 0.87)";
export const lightWhite = "rgba(255, 255, 255, 0.54)";
export const gray200 = "#EEEEEE";
export const gray300 = "#E0E0E0";
export const gray400 = "#BDBDBD";
export const gray500 = "#9E9E9E";
export const gray600 = "rgb(118, 118, 118)";
export const cardBorder = "#9cd5e4";
export const colorPalette = {
  primary1Color: "#486298",
  primary2Color: "#788fc9",
  primary3Color: "#14386a",
  secondary1Color: "#ff5722",
  secondary2Color: "#f0846f",
  secondary3Color: "#ba5544",
  textColor: black,
  secondaryTextColor: black,
  alternateTextColor: white,
  canvasColor: "#f5f4f4",
  // borderColor: _colors.grey300,
  // disabledColor: (0, _colorManipulator.fade)(_colors.darkBlack, 0.3),
  pickerHeaderColor: "#f0846f"

  // clockCircleColor: (0, _colorManipulator.fade)(_colors.darkBlack, 0.07),

  // shadowColor: _colors.fullBlack
};
