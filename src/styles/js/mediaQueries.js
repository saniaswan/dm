export const maxmedia1024 = "@media (max-width: 1024px)";
export const minmedia767 = "@media (min-width: 767px)";
export const maxmedia767 = "@media (max-width: 767px)";
export const maxmedia620 = "@media (max-width: 620px)";
export const maxmedia520 = "@media (max-width: 520px)";
export const maxmedia420 = "@media (max-width: 420px)";
