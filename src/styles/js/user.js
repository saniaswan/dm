import { maxmedia767 } from "../js/mediaQueries";
import { maxmedia420 } from "../js/mediaQueries";
export const style = {
  orgAccount: {
    fontSize: 14,
    color: "#BDBDBD",
    lineHeight: "normal",
    paddingLeft:20,
    paddingRight:20
  },
  createUserName: {
    lineHeight: "normal",
    fontSize: 24,
    width: "100%",
    margin:0,
    textOverflow: "ellipsis",
    overflow: "hidden",

    whiteSpace: "nowrap",
    color: "#333"
  },
  userName: {
    lineHeight: "normal",
    fontSize: 22,
    textOverflow: "ellipsis",
    overflow: "hidden",

    whiteSpace: "nowrap",
    paddingLeft:20,
    paddingRight:20,
    color: "#333",
  
  },
  userDetailsHeading: {
    lineHeight: "normal",
    fontSize: 16,
    textOverflow: "ellipsis",
    overflow: "hidden",

    whiteSpace: "nowrap",
    paddingLeft:20,
    paddingRight:20,
    color: "#333"
  },
  UserEditSelectField:{
    width: "100%"
  },
  UserEditInputField:{
    width: "100%",
   // marginLeft: "20px",
    height: "70px",
    display:"block",
  },
  linkColor: {
    color: "#BDBDBD",
    marginRight: 10,
    textDecoration: "none"
  },
  innerCard: {
    padding: "25px 10px 5px 10px"
  },
  innerCardWithoutActions: {
    padding: "20px 10px 20px 10px"
  },
  innerCardWithoutActions2: {
    padding: "20px 0 30px 0"
  },
  userInnerCardWithoutActions: {
    padding: "20px 0 0 0"
  },
innerCardWrap: {
    boxShadow: 'rgba(97, 97, 97, 0.3) 0px 1px 1px 1px',
    [maxmedia767]: {
      marginBottom: "20px"
    }
  },
  userInnerCardWrap: {
      textAlign: "center",
    boxShadow: 'rgba(97, 97, 97, 0.3) 0px 1px 1px 1px',
    [maxmedia767]: {
      marginBottom: "20px",
    },
    cursor: "pointer"
  },
  actionWrape: {
    padding: "8px 0px"
  },
  wrapperBottomMargin: {
    marginBottom: 20
  },
  bottomMargin: {
    marginBottom: "20px",
    [maxmedia767]: {
      marginBottom: "0px"
    }
  }
};

export const RiskStyle = {
  marginLeftMob: {
    [maxmedia767]: {
      marginLeft: "25px"
    }
  },
  confirmedItemTitle: {
    lineHeight: "22px",
    fontSize: 20,
    width: "100%",
    minHeight: "44px"
  },
  dotdotStyle: {
    height: "55px",
    overflow: "hidden"
  }
};
