export const calanderStyle = {
  wrapeDefault: {
    border: "1px solid #788fc9",
    height: 112
  },
  button: {
    padding: "0px",
    height: "30px",
    lineHeight: "10px",
    minWidth: "60px",
    width: "60px",
  },
  label: {
    padding: "0px"
  },
  overDueLabel: {
    padding: "0px",
    color: "red"
  },
  innerText: {
    color: "#222",
    margin: " 0px 0px 0px 6px",
    fontWeight: "400",
    fontFamily: "Roboto",
    whiteSpace: "normal",
    minHeight: "40"
  },
  viewButton:{
    padding: "0px",
    height: "30px",
    lineHeight: "10px",
    minWidth: "60px",
    width: "60px",
    float: "right"
  }
};

export const popupStyle = {
  contentBody: {
    color: "#000",
    overflowY: "auto"
  },
  contentWrapper: {
    overflowY: "auto",
    maxWidth: 625
  },
  lightText: {
    marginLeft: 0,
    marginBottom: 0
  },
  iconWrape: {
    position: "absolute",
    right: 20
  },
  overflow: {
    overflowY: "auto"
  },
  popUpWidth: {
    maxWidth: 625
  },
  textBlock: {
    display: "inline-block"
  }
};
