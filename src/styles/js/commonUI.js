import {
  maxmedia520,
  maxmedia767
} from "./mediaQueries";
import { cardBorder } from "../theme/colors";

export const loaderStyle = {
  loaderBack: { background: "rgba(249, 249, 249, 0.8)" },
  loaderSpinnerPosition: {
    display: "inline-block",
    width: 68,
    height: 68,
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    margin: "auto",
    maxWidth: 100,
    maxHheight: 100,
    overflow: "hidden",
    textAlign: "center"
  }
};

export const PopMessageStyle = {
  padding: "8px 24px",
  height: "auto",
  lineHeight: "17px"
};

export const appbarStyle = {
  iconStyle: {
    marginTop: 2
  },
  barHeight: {
    height: "100px",
    zIndex: 499,
    [maxmedia767]: {
      height: "50px"
    }
  }
};

export const logoStyle = {
  paddingBottom: "80px",
  paddingTop: "40px",
  width: "250px",
  [maxmedia520]: { paddingBottom: "40px", paddingTop: "20px", width: "200px" }
};

export const logoStyleMobile = {
  paddingBottom: "40px",
  paddingTop: "40px",
  width: "130px",
  [maxmedia520]: { paddingBottom: "40px", paddingTop: "20px", width: "100px" }
};

export const menuStyle = {
  menuCrossBtn: {
    height: "100px",
    background: cardBorder,
    [maxmedia767]: { height: "97px" }
  },
  menuTitleItems: {
    fontWeight: "bold",
    fontSize: "16px"
  },
  menuTitleItemMargin: {
    marginLeft: "16px",
    marginTop: "20px"
  },
  menuSpan: {
    fontSize: "12px",
    display: "block"
  },
  menuPosition: {
    top: "0px",
    left: "0px",
    bottom: "0px",
    width: "320px",
    overflowX: "hidden"
  },
  menuWidth: {
    width: "320px"
  },
  menuContainer: {
    overflow: "inherite"
  },
  menuDivider: {
    width: "85%",
    marginLeft: "20px"
  }
};
export const appDeleteModel = {
  cancelBtn: {
    top: "14px"
  }
};
