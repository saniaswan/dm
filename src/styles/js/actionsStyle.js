export const actionStyle = {
  dateComplete: {
    fontWeight: "bold",
    color: "green"
  },
  dateOverdue: {
    fontWeight: "bold",
    color: "red"
  },
  dateUpcoming: {
    fontWeight: "bold",
    color: "#486298"
  },
  riskStyle: {
    marginLeft: 0
  },
  wrap: {
    padding: "15px 10px 5px",
    cursor: "pointer"
  },
  label: {
    padding: "0px"
  },
  button: {
    padding: "0px",
    height: "30px",
    lineHeight: "10px",
    minWidth: "60px",
    width: "60px",
    float: "right"
  }
};
