import {
  minmedia767,
  maxmedia767,
  maxmedia620,
  maxmedia520,
  maxmedia420
} from "../js/mediaQueries";
import { colorPalette } from "../theme/colors";
import typography from "../theme/typography";
import { gray600 } from "../theme/colors";

export const tabPanelPosition = {
  position: "relative",
  top: "-47px",
  zIndex: 9991,
  [maxmedia767]: {
    top: "-75px"
  }
};

export const tabPanelTabSpacing = {
  minHeight: "500px",
  padding: "50px 20px",
  [maxmedia520]: {
    padding: "30px 20px"
  },
  [maxmedia420]: {
    padding: "20px 20px"
  }
};

export const registrationTextBox = {
  width: 250,
  marginRight: 20,
  marginTop: "-17px",
  [minmedia767]: {
    float: "right"
  },
  [maxmedia767]: {
    float: "right"
  },
  [maxmedia620]: {
    width: 210
  },
  [maxmedia520]: {
    margin: "0px auto",
    width: 270,
    float: "none"
  }
};

export const changePasswordTextBox = {
  width: 250,
  marginRight: 20,
  marginTop: "-17px",
  [minmedia767]: {
    float: "right"
  },
  [maxmedia767]: {
    float: "right"
  },
  [maxmedia520]: {
    margin: "0px auto",
    width: 280,
    float: "none"
  }
};

export const registrationContainer = {
  [maxmedia520]: {
    width: "100%"
  }
};

export const registrationBox = {
  width: 255,
  [maxmedia767]: {
    position: "relative",
    right: 30
  },
  [maxmedia520]: {
    margin: "0px auto",
    right: "initial",
    width: 270
  }
};

export const subHeading = {
  fontSize: typography.subHeadingFontSizeDesktop,
  color: gray600,
  width: "100%",
  position: "relative",
  lineHeight: "18px",
  [maxmedia520]: {
    fontSize: typography.subHeadingFontSizeDevice,
    lineHeight: "20px"
  }
};

export const borderBotom = {
  borderBottom: "1px solid rgba(212, 210, 210, 0.38)"
};

export const minHeight = {
  minHeight: 760
};

export const inkStyle = {
  backgroundColor: colorPalette.secondary2Color
};
