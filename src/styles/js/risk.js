import { colorPalette, gray400, gray200, gray300 } from "../theme/colors";
import { maxmedia767 } from "./mediaQueries";

export const riskStyle = {
  riskIndecators: { display: "inline-block" },
  riskActive: { color: colorPalette.primary2Color },
  riskUnderLine: {
    borderBottom: "1px solid #222",
    borderColor: gray400,
    width: 100,
    display: "inline-block",
    margin: "0px 10px"
  },
  riskQuestionBlock: {
    borderLeft: "1px solid #222",
    borderColor: gray400,
    padding: "30px 0px 0px 30px"
  },
  addQuestionIcon: {
    position: "absolute",
    right: "20px",
    bottom: "22px"
  },
  addQuestionIconBlock: {
    marginTop: 50,
    position: "relative",
    paddingBottom: 50
  },
  addQuestionMainBlock: {
    padding: "50px 20px 70px 20px",
    background: gray200
  },
  addQuestionField: {
    paddingTop: "20px",
    marginBottom: 30
  },
  otherField: {
    position: "relative",
    top: "-15px",
    marginTop: '15px',
  }
};

export const riskComment = {
  commentIcon: {
    display: "inline-block",
    position: "absolute",
    top: 35,
    right: 0
  },
  fieldRightPadding: {
    paddingRight: 25
  },
  mainPosition: {
    marginTop: "-20px"
  }
};

export const riskFile = {
  imageWrap: {
    width: 50,
    height: 50,
    overflow: "hidden",
    float: "left"
  },
  inputStyle: {
    cursor: "pointer",
    position: "absolute",
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
    width: "100%",
    opacity: 0
  },
  button: {
    margin: "5px 12px 12px 0px"
  },
  removeIcon: {
    width: 20,
    height: 20
  },
  removeIconWraper: {
    position: "relative",
    top: "-8px",
    right: 10
  }
};

export const riskForm = {
  toggleStyle: {
    color: gray300
  }
};

export const confirmedRisk = {
  changeRiskBtn: {
    position: "absolute",
    right: 20,
    [maxmedia767]: {
      bottom: "-50px"
    }
  },
  inBarButton: {
    float: "right",
    marginTop: "-4px"
  },
  cardTitle: {
    fontSize: 20,
    fontWeight: 100,
    color: "white"
  },
  cardTitlePosition: {
    paddingRight: 10,
    width: "50%"
  }
};

export const riskReportViewStyle = {
  wrapeRisk: {
    marginTop: 25
  }
};


export const riskAssessmentStyle = {
  headingIcon: {
    display: "inline-block",
    width: "60px",
    textDecoration: "none"
  },
  headingText: {
    width: "90%",
    display: "inline-block"
  },
  subheadingText: {
    marginRight: 10,
    textDecoration: 'underline'
  },
  arrowSign: {
    display: 'inline-block',
    marginRight: 10
  },
  headingIconPostion: {
    top: "5px",
    position: "relative"
  },
  whiteSubHeadingPosition: {
    color: "white",
    marginLeft: "63px",
    marginTop: "-20px",
    width: "75%",
    display: "block"
  },
  infoIconWrape: {
    marginLeft: 8,
    marginTop: 2,
    verticalAlign: 'middle',
    display:"inline-block"
    // top: 5,
    // right: 0,
    // position: "absolute",
    // [maxmedia767]: {
    //   top: "-22px",
    // }
  }

};
