import axios from "axios";
import { Messages } from "../utilities/Messages";
const configureAxiosDefaults = () => {
  /* attach token to each request header if request is other than login/register */
  axios.interceptors.request.use(
    cfg => {
      cfg.headers.Pragma = "no-cache";
      let url = cfg.url.toLowerCase();
      if (
        url.endsWith(Messages.citeria.login) ||
        url.endsWith(Messages.citeria.register) ||
        url.endsWith(Messages.citeria.resetpassword)
      )
        return cfg;

      if (getAuthToken === null) return cfg;

      const token = getAuthToken();
      if (token === null) return cfg;

      cfg.headers.common[Messages.headers.Authorization] = `Bearer ${token}`;
      return cfg;
    },
    err => {
      return Promise.reject(err);
    }
  );

  axios.interceptors.request.use(cfg => {
    cfg.data = cfg.data;
    return cfg;
  });
};

const getAuthToken = () => {
  try {
    const state = localStorage.getItem(Messages.citeria.state);
    if (state) {
      return state;
    } else {
      return null;
    }
  } catch (err) {
    return null;
  }
};
export default configureAxiosDefaults;
