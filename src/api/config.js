const ROOT_URL = "http://localhost:44378/api/";

export const config = {
  REST_API: {
    Account: {
      Login: `${ROOT_URL}Account/Login`,
      Register: `${ROOT_URL}Account/Register`,
      ResetPassword: `${ROOT_URL}Account/ForgotPassword`,
      ChangePassword: `${ROOT_URL}Account/ResetPassword`,
      ValidateResetToken: `${ROOT_URL}Account/ValidateResetToken`,
      ValidateLoggedUser: `${ROOT_URL}Account/ValidateLoggedUser`,
      ActivateUser: `${ROOT_URL}Account/ActivateUser`
    },

    Organisation: {
      Organisation: `${ROOT_URL}Organization`
    },
    Location: {
      CreateLocation: `${ROOT_URL}Location`,
      GetOrganisationAssociatedLocations: `${ROOT_URL}Location/`,
      DeleteLocation: `${ROOT_URL}Location/`,
      UpdateLocation: `${ROOT_URL}Location/`
    },
    Category: {
      GetCategories: `${ROOT_URL}Category/GetCategories`
    },
    Hazard: {
      GetCategoryAssociatedHazards: `${ROOT_URL}Hazard/GetHazards`
    },
    ActivityCalendar: {
      GetActivityCalendars: `${ROOT_URL}RiskAssesmentCMActions/GetActions`,
      CompleteAction: `${ROOT_URL}RiskAssesmentCMActions/UpdateActionEntity`,
      GetGroupedActions: `${ROOT_URL}RiskAssesmentCMActions/GetGroupedActions`
    },
    Risk: {
      GetHazardsAssociatedRisks: `${ROOT_URL}Risk/GetRisks?`,
      AddRisk: `${ROOT_URL}LocationRisks/AddMultipleRisks`
    },
    RiskAssessment: {
      PostWhoMightBeHarmedItem: `${ROOT_URL}/RiskAssessmentHarmed/AddHarmedEntries`,
      PostWhereIsEffectedItem: `${ROOT_URL}/RiskAssessmentAffected/AddAffectedEntries`,

      GetLocationRiskAssesmentLevel: `${ROOT_URL}/LocationRisks/GetLocationRiskDetails`,
      PostRiskLevel: `${ROOT_URL}/RiskAssessmentLevels`
    },
    RiskAssessmentReport: {
      GetReport: `${ROOT_URL}/Category/GetReport?`,
      GenerateRiskPDF: `${ROOT_URL}RiskAssessmentPdf/SaveAssessPdfReport`
    },
    LocationRisk: {
      GetLocationRisks: `${ROOT_URL}LocationRisks/GetLocationRisk?`
    },
    RiskAssesmentControlMeasures: {
      GetRiskAssesControlMeasureByLocationRiskId: `${ROOT_URL}RiskAssesmentControlMeasures/Get?locationRiskId=`,
      AddCustomControlMeasure: `${ROOT_URL}ControlMeasure`,
      DeleteCustomControlMeasure: `${ROOT_URL}ControlMeasure/DeleteCustomControlMeasure`,
      SaveRiskAssesControlMeasure: `${ROOT_URL}RiskAssesmentControlMeasures/AddRiskAssesControlMeasure`,
      DeleteArtifact: `${ROOT_URL}RiskAssesmentControlMeasures/DeleteArtifact/?`
    },
    S3Helper: {
      SetPreSignedUrl: `${ROOT_URL}Artifact/GetAritfact?`
    },
    DataDumper: {
      UploadData: `${ROOT_URL}DataDumper/DumpDataInDB?FilePath=`
    },
    ExceptionLogger: {
      LogError: `${ROOT_URL}ExceptionLogger/LogError`
    },
    User: {
      GetUsers: `${ROOT_URL}User/GetFilterData`,
      CreateUser: `${ROOT_URL}User/CreateUser`,
      UpdateUser: `${ROOT_URL}User/UpdateUser`,
      DeleteUser: `${ROOT_URL}User/DeleteUser`
    },
    RedirectUrl: {
      GetPreRequiredDataForActivityCalander: `${ROOT_URL}RedirectUrl/GetActivityCalanderData`
    }
  }
};
