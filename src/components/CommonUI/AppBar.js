import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import AppBar from "material-ui/AppBar";
import { appbarStyle } from "../../styles/js/commonUI";
import "../../styles/css/commonStyle.css";
import { onLogoutClick } from "../../redux/Actions/LoginActions";
import AppMenu from "./AppMenu";
import IconButton from "material-ui/IconButton";
import NavigationMenu from "material-ui/svg-icons/navigation/menu";
import Radium from "radium";
import { Global } from "../../utilities/Global";
import { isAllowToCreate } from "../../utilities";

class MainBar extends Component {
  state = {
    openMenu: false
  };

  constructor(props) {
    super(props);

    this.onHandleLogout = this.onHandleLogout.bind(this);
    this.handleOpenMenu = this.handleOpenMenu.bind(this);
    this.handleOnRequestChange = this.handleOnRequestChange.bind(this);
    this.openDelay = this.openDelay.bind(this);
  }

  static contextTypes = {
    router: PropTypes.func.isRequired
  };

  onHandleLogout = () => {
    this.handleCloseMenu();
    this.props.onLogoutClick();
    this.context.router.history.push("/");
  };

  handleOpenMenu = () => {
    let bar = document.getElementsByClassName("appBar")[0];
    bar.style.zIndex = 9999;
    if (
      navigator.appName === "Microsoft Internet Explorer" ||
      (navigator.userAgent.match(/Trident/) ||
        navigator.userAgent.match(/rv:11/)) ||
      /Edge\/\d./i.test(navigator.userAgent)
    )
      setTimeout(this.openDelay, 420);
    else
      this.setState({
        openMenu: true
      });
  };

  openDelay() {
    this.setState({
      openMenu: true
    });
  }

  handleCloseMenu = () => {
    if (window.innerWidth > 767) {
      let bar = document.getElementsByClassName("appBar")[0];
      bar.style.zIndex = 147;
    }

    this.setState({
      openMenu: false
    });
  };

  handleOnRequestChange = value => {
    this.setState({
      openMenu: value
    });
  };

  render() {
    const {
      organizationName,
      locationName,
      showRiskAssesment,
      riskAssesmentUri,
      isAuthenticate,
      isRegistered,
      appOrgId,
      appLocId,
      currrentUser
    } = this.props;
    let orgName = Global.truncate(organizationName);
    let locName = Global.truncate(locationName);
    return (
      <AppBar
        title=""
        style={appbarStyle.barHeight}
        className="appBar"
        iconElementLeft={
          <div>
            {isAuthenticate || isRegistered ? (
              <div>
                <IconButton
                  onTouchTap={this.handleOpenMenu}
                  iconStyle={{ color: "white" }}
                >
                  <NavigationMenu />
                </IconButton>
                <AppMenu
                  open={this.state.openMenu}
                  onHandleCloseMenu={this.handleCloseMenu}
                  onHandleLogout={this.onHandleLogout}
                  organization={orgName}
                  location={locName}
                  showRiskAssesment={showRiskAssesment}
                  riskAssesmentUri={riskAssesmentUri}
                  orgId={appOrgId}
                  locId={appLocId}
                  showHideCreateUser={isAllowToCreate(currrentUser)}
                />
              </div>
            ) : (
              " "
            )}{" "}
          </div>
        }
        iconStyleRight={appbarStyle.iconStyle}
        iconStyleLeft={appbarStyle.iconStyle}
      />
    );
  }
}

const mapStateToProps = ({
  appMenuBar,
  login,
  registration,
  genericReducer
}) => {
  const { isAuthenticate, currrentUser } = login;
  const { isRegistered } = registration;
  const { appOrgId, appLocId } = genericReducer;
  const {
    organizationName,
    locationName,
    showRiskAssesment,
    riskAssesmentUri
  } = appMenuBar;
  return {
    organizationName,
    locationName,
    showRiskAssesment,
    riskAssesmentUri,
    isAuthenticate,
    currrentUser,
    isRegistered,
    appOrgId,
    appLocId
  };
};

MainBar.propTypes = {
  organizationName: PropTypes.string.isRequired,
  locationName: PropTypes.string.isRequired,
  showRiskAssesment: PropTypes.bool.isRequired,
  riskAssesmentUri: PropTypes.string.isRequired,
  isAuthenticate: PropTypes.bool.isRequired,
  currrentUser: PropTypes.object,
  isRegistered: PropTypes.bool.isRequired,
  appOrgId: PropTypes.number.isRequired,
  appLocId: PropTypes.number.isRequired
};

export default connect(mapStateToProps, { onLogoutClick })(Radium(MainBar));
