import React, { Component } from "react";
import Radium from "radium";
import IconButton from "material-ui/IconButton";
import ActionInfo from "material-ui/svg-icons/action/info";
import { extraSmall } from "../../styles/commonStyle";
import NavigationClose from "material-ui/svg-icons/navigation/close";
import Dialog from "material-ui/Dialog";
import renderHTML from "react-render-html";
import {
  title1,
  flatIconButtonSmall,
  subHeadingBlack,
  mediumIcon
} from "../../styles/commonStyle";
import { popupStyle } from "../../styles/js/activityCalander";

class Information extends Component {
  constructor(props) {
    super(props);

    this.infoIconClick = this.infoIconClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.openDelay = this.openDelay.bind(this);
    this.state = {
      open: false
    };
  }

  handleClose() {
    let bar = document.getElementById("mainAppContainer");
    bar.style.zIndex = 9991;

    this.setState({ open: false });
  }

  openDelay() {
    this.setState({
      open: true
    });
  }

  infoIconClick() {
    let bar = document.getElementById("mainAppContainer");
    bar.style.zIndex = 114;
    if (
      navigator.appName === "Microsoft Internet Explorer" ||
      (navigator.userAgent.match(/Trident/) ||
        navigator.userAgent.match(/rv:11/)) ||
      /Edge\/\d./i.test(navigator.userAgent)
    )
      setTimeout(this.openDelay(), 420);
    else
      this.setState({
        open: true
      });
  }

  render() {
    return (
      <div>
        <IconButton
          iconStyle={mediumIcon.Icon}
          style={extraSmall.small}
          onClick={this.infoIconClick}
        >
          <ActionInfo />
        </IconButton>

        <Dialog
          modal={false}
          open={this.state.open}
          onRequestClose={this.handleClose}
          contentStyle={popupStyle.contentWrapper}
          bodyStyle={popupStyle.contentBody}
          autoScrollBodyContent={true}
        >
          <div style={popupStyle.iconWrape}>
            <IconButton
              style={flatIconButtonSmall}
              onTouchTap={this.handleClose}
            >
              <NavigationClose />
            </IconButton>
          </div>
          <h5>More information</h5>
          <h3 style={title1}>
            <b>{this.props.title}</b>
          </h3>

          <br />
          <p style={subHeadingBlack}>
            {this.props.comments && this.props.comments.length > 0
              ? renderHTML(this.props.comments)
              : ""}
          </p>
          <br />
        </Dialog>
      </div>
    );
  }
}

export default Radium(Information);
