import React from "react";
import PropTypes from "prop-types";
import Drawer from "material-ui/Drawer";
import MenuItem from "material-ui/MenuItem";
import IconButton from "material-ui/IconButton";
import NavigationClose from "material-ui/svg-icons/navigation/close";
import Menu from "material-ui/Menu";
import CommunicationBusiness
  from "material-ui/svg-icons/communication/business";
import ContentContentPaste from "material-ui/svg-icons/content/content-paste";
import ActionList from "material-ui/svg-icons/action/list";
import ActionSupervisorAccount
  from "material-ui/svg-icons/action/supervisor-account";
import ActionPowerSettingsNew
  from "material-ui/svg-icons/action/power-settings-new";
import ActionDateRange from "material-ui/svg-icons/action/date-range";

import { menuStyle } from "../../styles/js/commonUI";
import Radium from "radium";
import { Link } from "react-router-dom";
import "../../styles/css/commonStyle.css";
import Divider from "material-ui/Divider";
//import { Global } from "../../utilities/Global";

const AppMenu = ({
  open,
  onHandleCloseMenu,
  onHandleLogout,
  organization,
  location,
  showRiskAssesment,
  riskAssesmentUri,
  orgId,
  locId,
  showHideCreateUser
}) => (
  <Drawer
    width={320}
    open={open}
    className="appDrawer"
    docked={false}
    onRequestChange={onHandleCloseMenu}
    containerStyle={menuStyle.menuContainer}
  >
    <div style={menuStyle.menuCrossBtn}>
      <IconButton onTouchTap={onHandleCloseMenu}>
        <NavigationClose />
      </IconButton>
    </div>
    <Menu style={menuStyle.menuWidth} className="appMenu">
      <p style={[menuStyle.menuTitleItems, menuStyle.menuTitleItemMargin]}>
        Menu
      </p>
      {showHideCreateUser
        ? <MenuItem
            onTouchTap={onHandleCloseMenu}
            primaryText="User List"
            leftIcon={<ActionSupervisorAccount />}
            containerElement={<Link to="/userdashboard" />}
          />
        : <div />}

      <MenuItem
        onTouchTap={onHandleCloseMenu}
        containerElement={<Link to="/organisations" />}
        primaryText="Organisations"
        leftIcon={<CommunicationBusiness />}
      />
      <Divider style={menuStyle.menuDivider} />
      <p style={[menuStyle.menuTitleItems, menuStyle.menuTitleItemMargin]}>
        <span style={menuStyle.menuSpan}>{organization}</span>
        {location}
      </p>

      {showRiskAssesment
        ? <span>
            {showHideCreateUser
              ? <MenuItem
                  onTouchTap={onHandleCloseMenu}
                  containerElement={<Link to={riskAssesmentUri} />}
                  primaryText="Risk Assesment"
                  leftIcon={<ActionList />}
                />
              : <div />}
            {showHideCreateUser
              ? <MenuItem
                  onTouchTap={onHandleCloseMenu}
                  primaryText="Reports"
                  leftIcon={<ContentContentPaste />}
                  containerElement={
                    <Link to={`/riskassessmentreport/${orgId}/${locId}`} />
                  }
                />
              : <div />}
            {/*Will be implemented after MVP  */}
            <MenuItem
              onTouchTap={onHandleCloseMenu}
              primaryText="Action Items"
              leftIcon={<ActionDateRange />}
              containerElement={
                <Link to={`/activitycalander/${orgId}/${locId}`} />
              }
            />
          </span>
        : <p />}

      <MenuItem
        onTouchTap={onHandleLogout}
        primaryText="Log Out"
        leftIcon={<ActionPowerSettingsNew />}
      />
    </Menu>
  </Drawer>
);

AppMenu.propTypes = {
  open: PropTypes.bool.isRequired,
  onHandleCloseMenu: PropTypes.func.isRequired,
  onHandleLogout: PropTypes.func.isRequired,
  organization: PropTypes.string.isRequired,
  location: PropTypes.string.isRequired,
  showRiskAssesment: PropTypes.bool.isRequired,
  riskAssesmentUri: PropTypes.string.isRequired,
  orgId: PropTypes.number.isRequired,
  locId: PropTypes.number.isRequired,
  showHideCreateUser: PropTypes.bool.isRequired
};

export default Radium(AppMenu);
