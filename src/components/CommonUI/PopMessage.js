import React from "react";
import Snackbar from "material-ui/Snackbar";
import { PopMessageStyle } from "../../styles/js/commonUI";
export default class PopMessage extends React.Component {
  render() {
    return (
      <Snackbar
        open={this.props.open}
        message={this.props.message}
        autoHideDuration={5000}
        bodyStyle={PopMessageStyle}
      />
    );
  }
}
