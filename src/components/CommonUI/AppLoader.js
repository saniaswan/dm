import React, { Component } from "react";
import PropTypes from "prop-types";
import CircularProgress from "material-ui/CircularProgress";
import { overlayAll } from "../../styles/commonStyle";
import { loaderStyle } from "../../styles/js/commonUI";
class AppLoader extends Component {
  render() {
    let style = this.props.display
      ? { display: "inline-block" }
      : { display: "none" };

    return (
      <div style={Object.assign(overlayAll, loaderStyle.loaderBack, style)}>
        <CircularProgress
          size={this.props.size}
          thickness={this.props.thinkness}
          style={loaderStyle.loaderSpinnerPosition}
        />
      </div>
    );
  }
}

AppLoader.defaultProps = {
  size: 50,
  thinkness: 5
};

AppLoader.propTypes = {
  size: PropTypes.number.isRequired,
  thinkness: PropTypes.number.isRequired
};

export default AppLoader;
