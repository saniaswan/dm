import React from "react";
import Radium from "radium";
import { Link } from "react-router-dom";
import { logoStyle, logoStyleMobile } from "../../styles/js/commonUI";
import { displayNoneOnLow, displayNoneOnHigh } from "../../styles/commonStyle";
import { RouteNames } from "../../utilities";
const Logo = props => (
  <Link
    to="/"
    >
    <img
      src={require("../../images/Logo.png")}
      alt="logo"
      style={[logoStyle, displayNoneOnLow]}
      width={props.width}
      />
    <img
      src={require("../../images/mobile_logo.png")}
      alt="logo"
      style={[logoStyleMobile, displayNoneOnHigh]}
      width={props.width}
      />
  </Link>
);

export default Radium(Logo);
