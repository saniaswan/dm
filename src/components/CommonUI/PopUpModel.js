import React from "react";
import Dialog from "material-ui/Dialog";
import FlatButton from "material-ui/FlatButton";
/**
 * Alerts are urgent interruptions, requiring acknowledgement, that inform the user about a situation.
 */
export default class PopUpModel extends React.Component {
  render() {
    const actions = [
      <FlatButton
        label="Cancel"
        primary={true}
        onTouchTap={this.props.onHandleClose}
      />,
      <FlatButton
        label="Delete"
        primary={true}
        onTouchTap={this.props.onHandleConfirmDelete}
      />
    ];

    return (
      <div>
        <Dialog
          actions={actions}
          modal={true}
          open={this.props.open}
          onRequestClose={this.props.onHandleClose}
        >
          Are you sure {this.props.message}
        </Dialog>
      </div>
    );
  }
}
