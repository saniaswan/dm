import React, { Component } from "react";
import PropTypes from "prop-types";
import CircularProgress from "material-ui/CircularProgress";

class InnerAppLoader extends Component {
  render() {
    let style = this.props.display
      ? { display: "inline-block" }
      : { display: "none" };

    return (
      <div style={style}>
        <CircularProgress
          size={this.props.size}
          thickness={this.props.thinkness}
        />
      </div>
    );
  }
}

InnerAppLoader.defaultProps = {
  size: 20,
  thinkness: 3
};

InnerAppLoader.propTypes = {
  size: PropTypes.number.isRequired,
  thinkness: PropTypes.number.isRequired
};

export default InnerAppLoader;
