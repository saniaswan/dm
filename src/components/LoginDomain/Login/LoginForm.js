import React from "react";
import { PropTypes } from "prop-types";
import RaisedButton from "material-ui/RaisedButton";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import { Messages } from "../../../utilities";


const LoginForm = ({ email, password, onLogin, onChange, loginError }) => {
  return (
    <ValidatorForm name="loginForm" onSubmit={onLogin} instantValidate={false}>
      <TextValidator
        name="email"
        hintText="Paul@example.com"
        value={email}
        floatingLabelText="Email"
        validators={["required", "isValidEmail"]}
        errorMessages={[
          `${Messages.validation.emailReq}`,
          `${Messages.validation.emailFormat}`
        ]}
        onChange={onChange}
        errorText={loginError}
        fullWidth={true}
        />
      <TextValidator
        name="password"
        hintText="******"
        value={password}
        type="password"
        floatingLabelText="Password"
        validators={["required"]}
        errorMessages={[`${Messages.validation.passReq}`]}
        onChange={onChange}
        errorText={loginError}
        fullWidth={true}
        />
      <br />
      <br />
      <RaisedButton
        label="Login"
        type="submit"
        secondary={true}
        className="pull-right"
        />
    </ValidatorForm>
  );
};

LoginForm.propTypes = {
  email: PropTypes.string.isRequired,
  password: PropTypes.string.isRequired,
  onLogin: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  loginError: PropTypes.string
};

export default LoginForm;
