import React, { Component } from "react";
import { PropTypes } from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Radium from "radium";
import * as userActions from "../../../redux/Actions/LoginActions";
import * as styles from "../../../styles/js/startTabPanel";
import "../../../styles/css/startTabPanel.css";
import { heading1 } from "../../../styles/commonStyle";
import LoginForm from "./LoginForm";
import Logo from "../../CommonUI/AppLogo";

class LoginTab extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      user: {
        email: "",
        password: ""
      }
    };
    this.updateUserState = this.updateUserState.bind(this);
    this.onLoginBtnClick = this.onLoginBtnClick.bind(this);
  }

  updateUserState(event) {
    const field = event.target.name;
    let user = this.state.user;
    user[field] = event.target.value;
    return this.setState({ user });
  }

  onLoginBtnClick(event) {
    const user = this.state.user;
    this.props.actions.onLoginClick(user);
  }

  render() {
    const { loginErrorMessage } = this.props;
    return (
      <div
        className="container"
        style={Object.assign(styles.tabPanelTabSpacing, styles.minHeight)}
      >
        <div className="row">
          <div className="col-xs-12 text-center">
            <Logo width={200} />
          </div>
          <div
            className="col-xs-6 col-sm-6"
            style={styles.registrationContainer}
          >
            <div style={styles.registrationTextBox}>
              <h2 style={heading1}>Login</h2>
              <p style={styles.subHeading}>Good to see you again.</p>
            </div>
          </div>
          <div
            className="col-xs-6 col-sm-6"
            style={styles.registrationContainer}
          >
            <div style={styles.registrationBox}>
              <LoginForm
                onChange={this.updateUserState}
                onLogin={this.onLoginBtnClick}
                email={this.state.user.email}
                password={this.state.user.password}
                loginError={loginErrorMessage}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

LoginTab.propTypes = {
  actions: PropTypes.object,
  loginErrorMessage: PropTypes.string
};

const mapStateToProps = ({ login }) => {
  const { loginErrorMessage } = login;
  return { loginErrorMessage };
};

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(userActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Radium(LoginTab));
