import React, { Component } from "react";
import { PropTypes } from "prop-types";
import Radium from "radium";
import { connect } from "react-redux";
import { Tabs, Tab } from "material-ui/Tabs";
import { Card } from "material-ui/Card";
import LoginTab from "./Login/LoginTab";

import * as styles from "../../styles/js/startTabPanel";
import "../../styles/css/startTabPanel.css";
import PopMessage from "../CommonUI/PopMessage";
import AppLoader from "../CommonUI/AppLoader";
import DocumentTitle from "react-document-title";


import { loginError } from "../../redux/Actions/LoginActions";
import { onResetLoadingErrorMessage } from "../../redux/Actions/GenericAction";

class LoginRegistrationPanel extends Component {
  constructor(props, context) {
    super(props, context);

    this.state = { resetEmailVal: "" };
    this.onResetActive = this.onResetActive.bind(this);
  }
  render() {
    return (
      <div>
        <DocumentTitle title="CHEQS" />
        <Card>
         
            
           
              <LoginTab highlight={this.props.isError} />
           
           
         
        </Card>
        <PopMessage open={this.props.isError} message={this.props.message} />
        <AppLoader display={this.props.loading} />
      </div>
    );
  }

  onResetActive(resetVal) {
    this.setState({ resetEmailVal: resetVal });
    this.props.onResetLoadingErrorMessage(); // reset the loading and error message
    this.props.resetPassView(false);
    this.props.resgisterEmailError(""); // remove field highlight from company email
    this.props.loginError(""); // remove field highlight from login email and password
  }
}

// const mapStateToProps = ({ genericReducer }) => {
//   const { loading, message, isError } = genericReducer;
//   return { loading, message, isError };
// };

// LoginRegistrationPanel.propTypes = {
//   isError: PropTypes.bool.isRequired,
//   message: PropTypes.string.isRequired,
//   loading: PropTypes.bool.isRequired
// };

// LoginRegistrationPanel = connect(mapStateToProps, {
 
//   loginError,
//   onResetLoadingErrorMessage
// })(Radium(LoginRegistrationPanel));

 export default LoginRegistrationPanel;
