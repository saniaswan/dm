import React, { Component } from "react";
import { PropTypes } from "prop-types";
import Radium from "radium";
import { connect } from "react-redux";
import RaisedButton from "material-ui/RaisedButton";
import { Card } from "material-ui/Card";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import * as styles from "../../styles/js/startTabPanel";
import {
  onChangePasswordClick,
  clearMessage
} from "../../redux/Actions/ChangePasswordAction";
import { onChangePasswordPreRender } from "../../redux/Actions/ChangePasswordAction";
import PopMessage from "../CommonUI/PopMessage";
import {
  containerPadding,
  fullWidth,
  cardHeader,
  heading1,
  appColorSecondary
} from "../../styles/commonStyle";

class ChangePassword extends Component {
  constructor(props, context) {
    super(props, context);
    this.changePasswordSubmit = this.changePasswordSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);

    this.state = {
      changePassowrdView: {
        NewPassword: "",
        ConfirmPassword: ""
      }
    };
  }
  componentWillMount() {
    const path = this.props.location.pathname;
    let resettoken = this.props.location.search;
    if (resettoken !== "") {
      const userId = resettoken.split("::")[0].split("code=")[1];
      const code = resettoken.split("::")[1];
      this.props.onChangePasswordPreRender(userId, code);
    }
    ValidatorForm.addValidationRule("isPasswordMatch", value => {
      if (value !== this.state.changePassowrdView.NewPassword) {
        return false;
      }
      return true;
    });
  }

  handleChange(event) {
    this.props.clearMessage();
    const { changePassowrdView } = this.state;
    changePassowrdView[event.target.name] = event.target.value;
    this.setState({ changePassowrdView });
  }

  changePasswordSubmit(event) {
    const { userId, resetCode } = this.props;
    const { changePassowrdView } = this.state;
    const resetPassword = {
      UserId: userId,
      ResetCode: resetCode,
      NewPassword: changePassowrdView.NewPassword,
      ConfirmPassword: changePassowrdView.ConfirmPassword
    };
    this.props.onChangePasswordClick(resetPassword);
  }

  render() {
    const { changePassowrdView } = this.state;

    return (
      <Card>
        <div
          className="container"
          style={Object.assign(containerPadding, fullWidth)}
        >
          <div className="row">
            <div
              className="col-xs-6 col-sm-6"
              style={styles.registrationContainer}
            >
              <div style={styles.registrationTextBox}>
                <h2 style={heading1}>Change Password</h2>
                <p style={styles.subHeading}>
                  Let's get a new password for you.
                </p>
              </div>
            </div>
            <div
              className="col-xs-6 col-sm-6"
              style={styles.registrationContainer}
            >
              <div style={styles.registrationBox}>
                <ValidatorForm
                  name="resetForm"
                  onSubmit={this.changePasswordSubmit.bind(this)}
                  instantValidate={false}
                >
                  <TextValidator
                    name="NewPassword"
                    hintText="***"
                    value={changePassowrdView.NewPassword}
                    floatingLabelText="Password"
                    type="password"
                    validators={["required", "isValidPassword"]}
                    errorMessages={[
                      "Please enter your password",
                      "Password must be at least 8 characters with at least one uppercase , one number and a special character"
                    ]}
                    onChange={this.handleChange}
                  />
                  <br />
                  <TextValidator
                    name="ConfirmPassword"
                    value={changePassowrdView.ConfirmPassword}
                    floatingLabelText="Confirm Password"
                    type="password"
                    validators={["required", "isPasswordMatch"]}
                    errorMessages={[
                      "Please confirm your password",
                      "Your passwords don't match, please try again"
                    ]}
                    onChange={this.handleChange}
                  />
                  <br />
                  <br />
                  <RaisedButton
                    label="CHANGE"
                    secondary={true}
                    className="pull-right"
                    type="submit"
                  />
                </ValidatorForm>
              </div>
            </div>
          </div>
        </div>

        <PopMessage
          open={this.props.isChangePasswordError}
          message={this.props.message}
        />
      </Card>
    );
  }
}
ChangePassword = Radium(ChangePassword);
const mapStateToProps = ({ changePassword }) => {
  const { isChangePasswordError, message, userId, resetCode } = changePassword;
  return {
    isChangePasswordError,
    message,
    userId,
    resetCode
  };
};

ChangePassword.propTypes = {
  isChangePasswordError: PropTypes.bool,
  message: PropTypes.string.isRequired,
  userId: PropTypes.number,
  resetCode: PropTypes.string,
  onChangePasswordClick: PropTypes.func,
  onChangePasswordPreRender: PropTypes.func,
  clearMessage: PropTypes.func
};

ChangePassword = connect(mapStateToProps, {
  onChangePasswordClick,
  onChangePasswordPreRender,
  clearMessage
})(ChangePassword);
export default ChangePassword;
