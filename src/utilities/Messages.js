export const Messages = {
  validation: {
    emailReq: "Please enter your email address",
    emailFormat: "Please enter a valid email address",
    passReq: "Please enter your password",
    passFormat: "Password must be at least 6 characters",
    confirmPassReq: "Please enter your confirm password",
    confirmPassNotMatch: "Confirm password should match password",
    changeConfirmPassReq: "Please confirm your password",
    changeConfirmPassNotMatch: "Your passwords don't match, please try again",
    compReq: "Please enter your company name",
    nameReq: "Please enter your name",
    nameFormat: "Please enter a valid name",
    notesAction: "Please enter notes for the action",
    actionByWHo: "Please select a user",
    byWenAction: "Please select a date for the action",
    invalidFile: "Please enter a valid image format - 'JPEG, PNG or GIF'",
    requiredLocationName: "Please enter a location name",
    otherHowMightBeHarmed: "Please enter other for Who might be harmed.",
    userNameReq: "Please enter a name",
    userEmailReq: "Please enter a valid email address",
    duplicateOption: "Value already exist"
  },
  apiResponse: {
    duplicateEmailReg: "Someone's already using that email.",
    invalidEmailPass: "Sorry, we don't recognise your email/password combination. Please try again",
    passwordChange: "Your password has been successfully changed",
    CreatePassword: "Your password has been successfully created",
    invalidToken: "Token is in-valid.",
    sessionExpired: "Your session has expired, please log in again.",
    exception: "Oops, something went wrong",
    DataNoAddedProperly: "Record not entered correctly",
    LocationRiskAlreadyExists: "Location risk exists navigate to confirmed risks"
  },
  roles: {
    AccountAdmin: "AccountAdmin",
    Admin: "Admin",
    Member: "Member"
  },
  citeria: {
    login: "login",
    register: "register",
    resetpassword: "resetpassword",
    state: "state"
  },
  headers: {
    Authorization: "Authorization"
  },
  validationType: {
    isPasswordMatch: "isPasswordMatch",
    isValidPassword: "isValidPassword"
  },
  RiskAssessment: {
    information: "You currently have no relevant risks for the <Hazard name> hazard. You can add risks by selecting the 'Change Risks' option above."
  }
};
