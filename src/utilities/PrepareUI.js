import { Messages } from "../utilities/Messages";
export const PrepareUI = (list, currentUser) => {
  if (list && list.length > 0 && currentUser) {
    let userId = currentUser.userId;
    let userRole = currentUser.role;
    let dataList = list;
    let isAllowCreate = false;
    let updatedDataList = dataList.map(function(obj) {
      if (userRole === Messages.roles.AccountAdmin) {
        return Object.assign({}, obj, {
          isShowIcons: true
        });
      } else if (userRole === Messages.roles.Admin) {
        if (obj.userId === userId) {
          return Object.assign({}, obj, {
            isShowIcons: true,
            isAllowCreate: true
          });
        } else {
          return Object.assign({}, obj, {
            isShowIcons: false,
            isAllowCreate: true
          });
        }
      } else {
        return Object.assign({}, obj, {
          isShowIcons: false,
          isAllowCreate: false
        });
      }
    });
    return updatedDataList;
  }
  return [];
};
export const isAllowToCreate = currentUser => {
  let isAllow = false;
  if (currentUser) {
    if (currentUser.role === "Admin" || currentUser.role === "AccountAdmin")
      isAllow = true;
  }
  return isAllow;
};
export const isMember = currentUser => {
  return currentUser && currentUser.role === "Member";
};
export const canEditAndDeleteMember = (userlist, currentUser) => {
  if (userlist && userlist.length > 0 && currentUser) {
    let userRole = currentUser.role;
    let dataList = userlist;
    let updatedDataList = dataList.map(function(obj) {
      if (userRole === Messages.roles.AccountAdmin) {
        return Object.assign({}, obj, {
          canEditAndDelete: true
        });
      } else {
        if (userRole === Messages.roles.Admin) {
          if (obj.role === 3) {
            return Object.assign({}, obj, {
              canEditAndDelete: true
            });
          } else {
            return Object.assign({}, obj, {
              canEditAndDelete: false
            });
          }
        }
      }
    });
    return updatedDataList;
  }
  return [];
};
