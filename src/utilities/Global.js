export const Global = {
  convertToInt(val) {
    return parseInt(val, 10);
  },
  getFormatedDateString(date) {
    if (date) {
      const year = date.getFullYear();
      const month = date.getMonth() + 1;
      const day = date.getDate();

      return `${year}/${month}/${day}`;
    }
    return date;
  },
  getDateFromString(dateString) {
    // date string must be in day/month/year
    if (dateString) {
      let splitDate = dateString.split("/");
      const year = this.convertToInt(splitDate[2]);
      const month = this.convertToInt(splitDate[1]) - 1;
      const day = this.convertToInt(splitDate[0]);
      return new Date(year, month, day);
    }
    return dateString;
  },
  truncate(targetString) {
    const maxAllowedLength = 30;
    if (targetString.length > maxAllowedLength)
      return targetString.substring(0, maxAllowedLength) + " ...";
    else return targetString;
  }
};
