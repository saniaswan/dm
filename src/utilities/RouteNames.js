export const RouteNames = {
  Login: {
    route: "/",
    roles: []
  },
  ChangePassword: {
    route: "/changePassword",
    roles: []
  },
  Dashboard: {
    route: "/dashboard",
    roles: []
  },
  Organisations: {
    route: "/organisations",
    roles: []
  },
  CreateOrganisations: {
    route: "/organisation/create",
    roles: []
  },
  EditOrganisations: {
    route: "/organisation/edit/:orgId",
    roles: []
  },
  Locations: {
    route: "/location/list/:id",
    roles: []
  },
  CreateLocations: {
    route: "/location/create",
    roles: []
  },
  EditLocations: {
    route: "/location/edit/:locId",
    roles: []
  },
  Categories: {
    route: "/categories/:orgId/:locId",
    roles: []
  },
  Hazards: {
    route: "/hazards/:orgId/:locId/:catId",
    roles: []
  },
  Risks: {
    route: "/risks/:orgId/:locId/:catId/:hazardId",
    roles: []
  },
  EditRisks: {
    route: "/editrisks/:orgId/:locId/:catId/:hazardId",
    roles: []
  },
  ConfirmRisks: {
    route: "/confirmrisks/:orgId/:locId/:catId/:hazardId",
    roles: []
  },
  ConfirmedRisks: {
    route: "/confirmedrisks/:orgId/:locId/:catId/:hazardId",
    roles: []
  },
  ActivityCalander: {
    route: "/activitycalander/:orgId/:locId",
    roles: []
  },
  CompletedActions: {
    route: "/completedactions/:orgId/:locId",
    roles: []
  },
  OverdueActions: {
    route: "/overdueactions/:orgId/:locId",
    roles: []
  },
  UpcomingActions: {
    route: "/upcomingactions/:orgId/:locId",
    roles: []
  },
  RiskAssesment: {
    route: "/riskassessment/:orgId/:locId/:catId/:hazardId/:locationRiskId",
    roles: []
  },
  RiskAssesmentReport: {
    route: "/riskassessmentreport/:orgId/:locId",
    roles: []
  },
  ImportDB: {
    route: "/importdata",
    roles: []
  },
  UserDashboard: {
    route: "/userdashboard",
    roles: []
  },
  CreatePassword: {
    route: "/createpassword/:id",
    roles: []
  }
};
