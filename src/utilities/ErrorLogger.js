import axios from "axios";
import { config } from "../api";
const { REST_API } = config;

export class ErrorLogger {
  static handleExceptions(ex) {
    if (ex.config) {
      this.logErrors(ex.stack, ex.config.url);
    } else {
      this.logErrors(ex.stack, "");
    }
  }

  static logErrors(errorMessage, url) {
    axios.post(REST_API.ExceptionLogger.LogError, {
      ErrorMessage: errorMessage,
      Url: url
    });
  }
}
