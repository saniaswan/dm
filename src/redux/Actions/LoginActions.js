import {
  LOGIN_ACTIONS,
  REGISTRATION_ACTIONS,
  GENERIC_ACTIONS,
  RESET_PASSWORD_ACTIONS
} from "../ActionTypes";
import axios from "axios";
import { config } from "../../api";
import LogRocket from "logrocket";
import { Messages, ErrorLogger } from "../../utilities";

const { REST_API } = config;

export const onLoginClick = user => {
  return dispatch => {
    dispatch({
      type: GENERIC_ACTIONS.SHOW_HIDE_LOADING_ERROR_MESSAGE,
      payload: {
        loading: true,
        message: "",
        isError: false
      }
    });
    axios
      .post(REST_API.Account.Login, {
        Email: user.email,
        Password: user.password
      })
      .then(response => {
        const baseModel = response.data;
        let isError = false;
        let message = "";
        if (baseModel.success) {
          localStorage.setItem("state", baseModel.data.access_token);
          localStorage.setItem("user", baseModel.data.user);
          LogRocket.identify(user.email, {});
          dispatch({
            type: LOGIN_ACTIONS.LOGIN_SUCCESS,
            payload: baseModel.data
          });
          //reset the status of reset password, so that already showing message may disappear on login.
          dispatch({
            type: RESET_PASSWORD_ACTIONS.RESET_PASSWORD_SUCCESS,
            payload: false
          });
        } else {
          isError = true;
          message = Messages.apiResponse.invalidEmailPass;
          dispatch({
            type: LOGIN_ACTIONS.LOGIN_ERROR,
            payload: " " // for highlighting field sending empty error apparently
          });
          dispatch({
            type: LOGIN_ACTIONS.LOGIN_FAILURE,
            payload: baseModel.data
          });
        }
        dispatch({
          type: GENERIC_ACTIONS.SHOW_HIDE_LOADING_ERROR_MESSAGE,
          payload: {
            loading: false,
            message: message,
            isError: isError
          }
        });
      })
      .catch(error => {
        ErrorLogger.handleExceptions(error);
        dispatch({
          type: GENERIC_ACTIONS.SHOW_HIDE_LOADING_ERROR_MESSAGE,
          payload: {
            loading: false,
            message: `${Messages.apiResponse.exception}`,
            isError: true
          }
        });
      });
  };
};

export const onLogoutClick = () => {
  return dispatch => {
    dispatch({
      type: GENERIC_ACTIONS.SHOW_HIDE_LOADING_ERROR_MESSAGE,
      payload: {
        loading: false,
        message: "",
        isError: false
      }
    });
    localStorage.removeItem("state");
    localStorage.removeItem("user");
    dispatch({ type: LOGIN_ACTIONS.LOGOUT_SUCCESS, payload: false });
    dispatch({ type: REGISTRATION_ACTIONS.IS_REGISTERED, payload: false });
  };
};

export const onCheckAuthorization = () => {
  return dispatch => {
    dispatch({
      type: GENERIC_ACTIONS.SHOW_HIDE_LOADING_ERROR_MESSAGE,
      payload: {
        loading: true,
        message: "",
        isError: false
      }
    });
    let accessToken = localStorage.getItem("state");
    if (accessToken != null) {
      axios
        .get(REST_API.Account.ValidateLoggedUser)
        .then(response => {
          let isError = false;
          let message = "";
          const baseModel = response.data;
          if (baseModel.success) {
            dispatch({
              type: LOGIN_ACTIONS.LOGIN_SUCCESS,
              payload: baseModel.data
            });
          } else {
            localStorage.removeItem("state");
            localStorage.removeItem("user");
            isError = true;
            message = Messages.apiResponse.sessionExpired;
          }
          dispatch({
            type: GENERIC_ACTIONS.SHOW_HIDE_LOADING_ERROR_MESSAGE,
            payload: {
              loading: false,
              message: message,
              isError: isError
            }
          });
        })
        .catch(error => {
          // Log Exception
          ErrorLogger.handleExceptions(error);
          dispatch({
            type: GENERIC_ACTIONS.SHOW_HIDE_LOADING_ERROR_MESSAGE,
            payload: {
              loading: false,
              message: `${Messages.apiResponse.sessionExpired}`,
              isError: true
            }
          });
        });
    } else {
      dispatch({
        type: GENERIC_ACTIONS.SHOW_HIDE_LOADING_ERROR_MESSAGE,
        payload: {
          loading: false,
          message: "",
          isError: false
        }
      });
    }
  };
};

export const loginError = value => {
  return dispatch => {
    dispatch({
      type: LOGIN_ACTIONS.LOGIN_ERROR,
      payload: value
    });
  };
};
