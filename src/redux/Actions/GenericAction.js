import { GENERIC_ACTIONS } from "../ActionTypes";

export const onResetLoadingErrorMessage = () => {
  return dispatch => {
    dispatch({
      type: GENERIC_ACTIONS.SHOW_HIDE_LOADING_ERROR_MESSAGE,
      payload: {
        loading: false,
        message: "",
        isError: false
      }
    });
  };
};

export const onSetOrgId = value => {
  return dispatch => {
    dispatch({
      type: GENERIC_ACTIONS.SET_ORGANISATION_ID,
      payload: value
    });
  };
};

export const onSetLocationId = value => {
  return dispatch => {
    dispatch({
      type: GENERIC_ACTIONS.SET_LOCATION_ID,
      payload: value
    });
  };
};

export const onSetCategoryId = value => {
  return dispatch => {
    dispatch({
      type: GENERIC_ACTIONS.SET_CATAGORY_ID,
      payload: value
    });
  };
};

export const onSetHazardId = value => {
  return dispatch => {
    dispatch({
      type: GENERIC_ACTIONS.SET_HAZARD_ID,
      payload: value
    });
  };
};

export const onSetLocationRiskId = value => {
  return dispatch => {
    dispatch({
      type: GENERIC_ACTIONS.SET_LOCATION_RISK_ID,
      payload: value
    });
  };
};

export const onSetRedirectUrl = value => {
  return dispatch => {
    dispatch({
      type: GENERIC_ACTIONS.SET_REDIRECT_URL,
      payload: value
    });
  };
};
