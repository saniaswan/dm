export const LOGIN_ACTIONS = {
  LOGIN_SUCCESS: "LOGIN_SUCCESS",
  LOGIN_FAILURE: "LOGIN_FAILURE",
  LOGOUT_SUCCESS: "LOGOUT_SUCCESS",
  LOGIN_ERROR: "LOGIN_ERROR"
};

export const REGISTRATION_ACTIONS = {
  REGISTRATION_SUCCESS: "REGISTRATION_SUCCESS",
  IS_REGISTERED: "IS_REGISTERED",
  REGISTERATION_ERROR: "REGISTER_ERROR",
  REGISTER_EMAIL_ERROR: "REGISTER_EMAIL_ERROR"
};

export const RESET_PASSWORD_ACTIONS = {
  RESET_PASSWORD_SUCCESS: "RESET_PASSWORD_SUCCESS"
};

export const START_TAB_PANEL_ACTIONS = {
  SHOW_ERROR: "SHOW_ERROR",
  SHOW_Hide_LOADING: "SHOW_Hide_LOADING"
};
export const CHANGE_PASSWORD_ACTIONS = {
  CHANGE_PASSWORD_SUCCESS: "CHANGE_PASSWORD_SUCCESS",
  CHANGE_PASSWORD_FAILURE: "CHANGE_PASSWORD_FAILURE",
  VALIDATE_RESET_TOKEN: "VALIDATE_RESET_TOKEN",
  RESET_PASSWORD_ERROR: "RESET_PASSWORD_ERROR",
  CHANGE_PASSWORD_SHOW_ERROR: "CHANGE_PASSWORD_SHOW_ERROR",
  CLEAR_MESSAGE: "CLEAR_MESSAGE",
  SHOW_HIDE_LOADING: "SHOW_HIDE_LOADING"
};

export const ORGANIZATION_ACTIONS = {
  CREATE_ORGANIZATION_SUCCESS: "CREATE_ORGANIZATION_SUCCESS",
  CREATE_ORGANIZATION_FAILURE: "CREATE_ORGANIZATION_FAILURE",
  GET_ORGANIZATIONS: "GET_ORGANIZATIONS",
  ORGANIZATION_LODAER: "ORGANIZATION_LODAER",
  DELETE_ORGANIZATION_SUCCESS: "DELETE_ORGANIZATION_SUCCESS",
  DELETE_ORGANIZATION_FAILURE: "DELETE_ORGANIZATION_FAILURE",
  UPDATE_ORGANIZATION_SUCCESS: "UPDATE_ORGANIZATION_SUCCESS"
};

export const LOCATION_ACTIONS = {
  CREATE_LOCATION_SUCCESS: "CREATE_LOCATION_SUCCESS",
  CREATE_LOCATION_FAILURE: "CREATE_LOCATION_FAILURE",
  GET_LOCATIONS: "GET_LOCATIONS",
  SET_ORGANIZATION_ID: "SET_ORGANIZATION_ID",
  DELETE_LOCATION_SUCCESS: "DELETE_LOCATION_SUCCESS",
  UPDATED_LOCATION_SUCESS: "UPDATED_LOCATION_SUCESS"
};

export const CATEGORY_ACTIONS = {
  GET_CATEGORIES: "GET_CATEGORIES"
};

export const HAZARD_ACTIONS = {
  GET_HAZARDS: "GET_HAZARDS",
  CHECK_ALL_HAZARDS_COMPLETED: "CHECK_ALL_HAZARDS_COMPLETED",
  ALL_HAZARDS_COMPLETED: "ALL_HAZARDS_COMPLETED",
  GET_HAZARD_ID: "GET_HAZARD_ID"
};

export const ACTIVITYCALENDAR_ACTIONS = {
  GET_ACTIVITIES: "GET_ACTIVITYCALENDARS",
  COMPLETE_ACTION: "COMPLETE_ACTION",
  GET_GROUPED_ACTIONS: "GET_GROUPED_ACTIONS",
  SET_GROUPED_ACTIONS_COUNT: " SET_GROUPED_ACTIONS_COUNT",
  SHOW_HIDE_COMPLETED_OPTIONS: "SHOW_HIDE_COMPLETED_OPTIONS",
  REDIRECT_BACK_TO_LOGIN: "REDIRECT_BACK_TO_LOGIN",
  LOAD_ACTIONS: "LOAD_ACTIONS"
};

export const RISK_ACTIONS = {
  GET_RISKS: "GET_RISKS",
  CHECK_CHANGE: "CHECK_CHANGE",
  ADD_RISKS: "ADD_RISKS",
  ADD_RISK_SUCCESS: "ADD_RISK_SUCCESS",
  ADD_RISK_FAILURE: "ADD_RISK_FAILURE",
  CONFIRM_RISK: "CONFIRM_RISK",
  SET_IS_RISK_ADD: "SET_IS_RISK_ADD",
  CHECK_RISKS: "CHECK_RISKS",
  EDIT_RISKS: "EDIT_RISKS",
  SELECTED_RISKS: "SELECTED_RISKS",
  IS_EDIT_RISK: "IS_EDIT_RISK",
  IS_CONFIRM_RISK_VIEW: "IS_CONFIRM_RISK_VIEW",
  CHECK_ALL_RISK_COMPLETED: "CHECK_ALL_RISK_COMPLETED",
  ALL_RISK_COMPLETED: "ALL_RISK_COMPLETED"
};

export const APP_MENU_ACTIONS = {
  SET_ORGANIZATION: "SET_ORGANIZATION",
  SET_LOCATION: "SET_LOCATION",
  SET_RISK_ASSESMENT: "SET_RISK_ASSESMENT",
  RESET_ALL: "RESET_ALL"
};
export const RISK_ASSESSMENT_ACTIONS = {
  GET_WHO_MIGHT_BE_HARMED_LIST: "GET_WHO_MIGHT_BE_HARMED_LIST",
  POST_WHO_MIGHT_HARMED_ITEM: "POST_WHO_MIGHT_HARMED_ITEM",
  WHO_MIGHT_BE_HARMED_SUCCESS: "WHO_MIGHT_BE_HARMED_SUCCESS",
  HARMED_ITEM_CHECKED_UNCHECKED: "HARMED_ITEM_CHECKED_UNCHECKED",
  IS_HARMED_ITEM_SELECTED: "IS_HARMED_ITEM_SELECTED",

  GET_WHERE_IS_EFFECTED_LIST: "GET_WHERE_IS_EFFECTED_LIST",
  GET_WHERE_IS_EFFECTED_SUCCESS: "GET_WHERE_IS_EFFECTED_SUCCESS",
  EFFECTED_ITEM_CHECKED_UNCHECKED: "EFFECTED_ITEM_CHECKED_UNCHECKED",
  IS_EFFECTED_ITEM_SELECTED: "IS_EFFECTED_ITEM_SELECTED",

  HANDLED_CONTINUE_BUTTON_DISABLED: "HANDLED_CONTINUE_BUTTON_DISABLED",
  GET_RISK_LEVEL_LIST: "GET_WHERE_IS_EFFECTED_LIST",
  RISK_LEVEL_SUCCESS: "RISK_LEVEL_SUCCESS",
  LEVELS_ITEM_CHECKED_UNCHECKED: "LEVELS_ITEM_CHECKED_UNCHECKED",
  IS_LEVELS_ITEM_SELECTED: "IS_LEVELS_ITEM_SELECTED",
  RISK_ASSESSMENT_FAILURE: "RISK_ASSESSMENT_FAILURE",
  RISK_ASSESSMENT_STEP_INDEX: "RISK_ASSESSMENT_STEP_INDEX"
};
export const RISK_ASSESSMENT_REPORT_ACTIONS = {
  GET_REPORT: "GET_REPORT"
};
export const LOCATION_RISK_ACTIONS = {
  GET_LOCATION_RISKS: "GET_LOCATION_RISKS"
};
export const GENERIC_ACTIONS = {
  SHOW_HIDE_LOADING_ERROR_MESSAGE: "SHOW_HIDE_LOADING_ERROR_MESSAGE",
  SET_ORGANISATION_ID: "SET_ORGANISATION_ID",
  SET_LOCATION_ID: "SET_LOCATION_ID",
  SET_CATAGORY_ID: "SET_CATAGORY_ID",
  SET_HAZARD_ID: "SET_HAZARD_ID",
  SET_LOCATION_RISK_ID: "SET_LOCATION_RISK_ID",
  SET_REDIRECT_URL: "SET_REDIRECT_URL"
};
export const RISK_ASSESSMENT_CONTROLMEASURE_ACTIONS = {
  GET_RISKASSESSMENT_CONTROL_MEASURE: "GET_RISKASSESSMENT_CONTROL_MEASURE",
  ADD_QUESTION_BLOCK: "ADD_QUESTION_BLOCK",
  ADD_NEWLY_ADDED_CUSTOM_MEASURE: "ADD_NEWLY_ADDED_CUSTOM_MEASURE",
  DELETE_CUSTOM_CONTROL_MEASURE_SUCCESS: "DELETE_CUSTOM_CONTROL_MEASURE_SUCCESS",
  ASSESSMENT_SAVED: "ASSESSMENT_SAVED",
  DELETE_ARTIFACT: "DELETE_ARTIFACT"
};

export const CREATE_PASSWORD_ACTIONS = {
  CREATE_PASSWORD_SUCCESS: "CREATE_PASSWORD_SUCCESS",
  CREATE_PASSWORD_FAILURE: "CREATE_PASSWORD_FAILURE"
};

export const USER_ACTIONS = {
  GET_USERS: "GET_USERS",
  SHOW_HIDE_CREATE_USER: "SHOW_HIDE_CREATE_USER",
  NEW_USER: "NEW_USER",
  CREATE_USER: "CREATE_USER",
  EDIT_USER: "EDIT_USER",
  EDIT_USER_CLICK: "EDIT_USER_CLICK",
  UPDATE_USER: "UPDATE_USER",
  DELETE_USER: "DELETE_USER"
};
