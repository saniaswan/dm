import { GENERIC_ACTIONS } from "../ActionTypes";

const INITIAL_STATE = {
  loading: false,
  message: "",
  isError: false,
  appOrgId: 0,
  appLocId: 0,
  appCatId: 0,
  appHazardId: 0,
  appLocationRiskId: 0,
  redirectUrl: null,
  isRedirectUrlInprogress: false
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GENERIC_ACTIONS.SHOW_HIDE_LOADING_ERROR_MESSAGE:
      return {
        ...state,
        loading: action.payload.loading,
        message: action.payload.message,
        isError: action.payload.isError
      };
    case GENERIC_ACTIONS.SET_ORGANISATION_ID:
      return {
        ...state,
        appOrgId: action.payload
      };
    case GENERIC_ACTIONS.SET_LOCATION_ID:
      return {
        ...state,
        appLocId: action.payload
      };
    case GENERIC_ACTIONS.SET_CATAGORY_ID:
      return {
        ...state,
        appCatId: action.payload
      };
    case GENERIC_ACTIONS.SET_HAZARD_ID:
      return {
        ...state,
        appHazardId: action.payload
      };
    case GENERIC_ACTIONS.SET_LOCATION_RISK_ID:
      return {
        ...state,
        appLocationRiskId: action.payload
      };
    case GENERIC_ACTIONS.SET_REDIRECT_URL:
      return {
        ...state,
        redirectUrl: action.payload,
        isRedirectUrlInprogress: action.payload ? true : false
      };
    default:
      return state;
  }
};
