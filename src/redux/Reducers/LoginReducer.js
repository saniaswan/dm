import { LOGIN_ACTIONS } from "../ActionTypes";

const INITIAL_STATE = {
  currrentUser: {},
  isAuthenticate: false,
  credentailsInvalid: false
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case LOGIN_ACTIONS.LOGIN_SUCCESS:
      let userObj = action.payload.user;
      if (userObj !== undefined) {
        userObj = action.payload.user;
      }
      return {
        ...state,
        currrentUser: userObj,
        isAuthenticate: true,
        credentailsInvalid: false
      };

    case LOGIN_ACTIONS.LOGIN_FAILURE:
      return {
        ...state,
        currrentUser: action.payload,
        isAuthenticate: false,
        credentailsInvalid: false
      };

    case LOGIN_ACTIONS.LOGOUT_SUCCESS:
      return {
        ...state,
        currrentUser: null,
        isAuthenticate: false,
        credentailsInvalid: false
      };

    case LOGIN_ACTIONS.LOGIN_ERROR:
      return {
        ...state,
        loginErrorMessage: action.payload
      };

    default:
      return state;
  }
};
