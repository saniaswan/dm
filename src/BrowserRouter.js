import React, { Component } from "react";
import { connect } from "react-redux";
import { BrowserRouter as Router, Route, Redirect } from "react-router-dom";
import LoginRegistrationPanel from "./components/LoginDomain/StartTabPanel";
import { appBodyContainer, appBase } from "./styles/commonStyle";

import { bindActionCreators } from "redux";
import * as userActions from "./redux/Actions/LoginActions";

import Radium from "radium";
import { RouteNames } from "./utilities";
import { Global } from "./utilities/Global";

class BrowserRouter extends Component {
  componentWillMount() {
    this.props.actions.onCheckAuthorization();
     
  }

  scrollToTop() {
    window.scroll(0, 0);
  }

  redirectUrl(route, routeProps) {
    if (route === RouteNames.ActivityCalander.route) {
      const { isAuthenticate, isRegistered, redirectUrl } = this.props;
      if (!(isAuthenticate || isRegistered) && redirectUrl === null) {
        // set redirect url
        this.props.genericAction.onSetRedirectUrl(routeProps.match.url);
      }
      return true;
    }
    return false;
  }

  render() {
    this.scrollToTop();
    const {
      isAuthenticate,
      currrentUser,
      isRegistered,
      message,
      isOrganisationCreated,
      isOrganisationUpdated,
      isLocationCreated,
      isLocationUpdated,
      orgId,
      riskAddSuccess,
      assessmentSaved,
      checkRisk,
      appOrgId,
      appLocId,
      appCatId,
      appHazardId,
      redirectUrl,
      allRiskCompleted,
      allHazardsCompleted,
      isPasswordCreate
    } = this.props;

    //TODO
    // Logged User Role
    const role =
      currrentUser && currrentUser.data
        ? currrentUser.data.user.role.toLocaleLowerCase()
        : "";
    return (
      <Router>
        <div style={appBase}>
         
          <div style={appBodyContainer} id="mainAppContainer">
            {/* Start of Non-Auth Routes */}
                   
            
            
            {/* End of Non-Auth Routes */}

            {/* Start of Auth Routes */}
            <Route
              exact
              path={RouteNames.Login.route}
              render={props =>
                isAuthenticate || isRegistered ? (
                  redirectUrl ? (
                    <Redirect {...props} to={redirectUrl} />
                  ) : (
                    <Redirect {...props} to={RouteNames.Organisations.route} />
                  )
                ) : (
                  <LoginRegistrationPanel {...props} />
                )
              }
            />
           

            
            
            {/* End of Auth Routes */}
          </div>
        </div>
      </Router>
    );
  }
}
const mapStateToProps = ({
  login
 
}) => {
  const { isAuthenticate, currrentUser } = login;
  
 
  return {
    isAuthenticate,
    currrentUser
    
  };
};

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(userActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(
  Radium(BrowserRouter)
);
