import {createStore, applyMiddleware} from 'redux';
import Reducer from '../redux/Reducers';
import ReduxThunk from 'redux-thunk';

export default function configureStore(initialState) {
  return createStore(
    Reducer,
    {},
    applyMiddleware(ReduxThunk)
  );
}
