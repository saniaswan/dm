import {createStore, applyMiddleware} from 'redux';
import Reducer from '../redux/Reducers';
import reduxImmutableStateInvariant from 'redux-immutable-state-invariant';/*Redux middleware that spits an error on you when you try to mutate your state either inside a dispatch or between dispatches. For development use only!*/
import ReduxThunk from 'redux-thunk';
import Raven from 'raven';
import createRavenMiddleware from 'raven-for-redux';
import LogRocket from 'logrocket';

export default function configureStore(initialState) {
  return createStore(
    Reducer,
    {},
    applyMiddleware(ReduxThunk, reduxImmutableStateInvariant(), LogRocket.reduxMiddleware(), createRavenMiddleware(Raven))
  );
}
